﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UWC.ServiceReference1;

namespace UWCNS
{
    class UWC
    {
        static string fileName = "file.txt";
        static string filePath = Directory.GetCurrentDirectory() + "\\" + fileName;

        static void Main(string[] args)
        {
            if (!File.Exists(filePath))
            {
                Console.WriteLine("File doesn't exist!");
            }

            using (FileStream stream = new FileStream(filePath, FileMode.Open))
            {
                using (Service1Client client = new Service1Client("BasicHttpBinding_IService1"))
                {
                    Dictionary<string, int> slovar = client.GetUniqueWordsFromText(stream);
                    using (StreamWriter streamWriter = new StreamWriter("wordsCount.txt"))
                    {
                        foreach (var kvp in slovar.OrderByDescending(x => x.Value))
                        {
                            streamWriter.WriteLine($"{kvp.Key} \t {kvp.Value}");
                        }
                    }
                }

                /*Reflection
                var myType = Type.GetType("WordCounter.Counter, WordCounter");

                var instance = (WordCounter.Counter)Activator.CreateInstance(myType);
                var methodInfo = instance.GetType().GetMethod("GetUniqueWords", BindingFlags.NonPublic | BindingFlags.Instance);
                var obj = (Dictionary<string, int>)methodInfo.Invoke(instance, new object[] { text });
                */

                //using (StreamWriter streamWriter = new StreamWriter("wordsCount.txt"))
                //{
                //    //Counter counter = new Counter(text, Counter.Mode.Threads);
                //    //foreach (var kvp in counter.GetUniqueWords().OrderByDescending(x => x.Value))
                //    //{
                //    //    streamWriter.WriteLine($"{kvp.Key} \t {kvp.Value}");
                //    //}
                //    Console.WriteLine("done");
                //}
            }
            Console.WriteLine("Done!!!");
            Console.ReadLine();
        }
    }
}
