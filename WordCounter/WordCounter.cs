﻿using System;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace WordCounter
{
    public class Counter
    {
        public enum Mode 
        {
            Linear,
            Parallel,
            Threads
        }

        string Text;

        char[] specSymbols = new char[] { ' ', '\r', '|', '*', ',', '.', '!', ';', '?', '-', '"', '[', ']', ':', '(', ')', '{', '}', '\t', '\n', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        Dictionary<string, int> words;

        ConcurrentDictionary<string, int> cWords;

        public Counter(string text, Mode mode = Mode.Linear)
        {
            Text = text;
            switch (mode) 
            {
                case Mode.Linear:
                    FillUniqueWords();
                    break;

                case Mode.Parallel:
                    cWords = new ConcurrentDictionary<string, int>();
                    FillUniqueWordsParallel();
                    words = new Dictionary<string, int>(cWords);
                    break;

                case Mode.Threads:
                    cWords = new ConcurrentDictionary<string, int>();
                    FillUniqueWordsThreads();
                    words = new Dictionary<string, int>(cWords);
                    break;

                default:
                    FillUniqueWords();
                    break;
            }   
            
        }

        public Dictionary<string, int> GetUniqueWords() { return words; }

        private void FillUniqueWords()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            foreach (var word in Text.Split(specSymbols, StringSplitOptions.RemoveEmptyEntries))
            {
                if (word == "")
                    continue;
                if (words.ContainsKey(word))
                {
                    ++words[word];
                }
                else
                {
                    words.Add(word, 1);
                }
            }            
            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

            Console.WriteLine("RunTime: " + elapsedTime);
        }

        private void FillUniqueWordsThreads() 
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int index = FindSpaceIndex();

            string fstPiece = Text.Substring(0, index);
            string secPiece = Text.Substring(index, Text.Length - index);
            
            Thread firstHalfThread = new Thread(p => ThreadJob((string)p));
            firstHalfThread.Start(fstPiece);

            Thread secondHalfThread = new Thread(p => ThreadJob((string)p));
            secondHalfThread.Start(secPiece);

            firstHalfThread.Join();
            secondHalfThread.Join();

            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

            Console.WriteLine("RunTime: " + elapsedTime);
        }
        
        private int FindSpaceIndex()
        {
            int index = Text.Length / 2;

            while(Text[index] != ' ')
            {
                if(index < Text.Length)
                {
                    index++;
                }
                else 
                {
                    Console.WriteLine("Cant finde space");
                    return 0;
                }
            }

            return index;
        }

        private void FillUniqueWordsParallel()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var lines = Text.Split('\n', '\r').Where(x => x.Length != 0);

            Parallel.ForEach(lines, ThreadJob);

            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

            Console.WriteLine("RunTime: " + elapsedTime);
        }

        private void ThreadJob(string pieceOfText)
        {
            foreach (var word in pieceOfText.Split(specSymbols, StringSplitOptions.RemoveEmptyEntries))
            {
                if (word == "")
                    continue;

                cWords.AddOrUpdate(word, 1, (key, value) => ++value );
            }
        }

        /* Reflection
        private Dictionary<string, int> GetUniqueWords(string Text)
        {
            Dictionary<string, int> words = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
            foreach (var word in Text.Split(specSymbols, StringSplitOptions.RemoveEmptyEntries))
            {
                if (word == "")
                    continue;
                if (words.ContainsKey(word))
                {
                    words[word]++;
                }
                else
                {
                    words.Add(word, 1);
                }
            }
            
            return words;
        }
        */
    }
}
